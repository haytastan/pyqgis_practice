home_path = QgsProject.instance().homePath()
layer = iface.activeLayer()
request = QgsFeatureRequest(QgsExpression("type='straight'"))
features = layer.getFeatures(request)
layer.selectByRect(rectangle)
for feature in features:
    print(f"{feature['name']}: {feature['type']}")