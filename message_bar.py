# Pops up a message bar entry
iface.messageBar().pushMessage('OSP Tools',
    'Plugin updates available',
    Qgis.Critical,
    0)