import os

from PyQt5.QtWidgets import QFrame, QGridLayout, QMainWindow
from qgis.gui import QgsMapCanvas
from qgis.core import QgsProject, QgsVectorLayer


class OurMainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        self.setupGui()
        self.project = QgsProject()

        self.add_ogr_layer('/data/pyqgis_data/alaska.shp')
        self.map_canvas.zoomToFullExtent()

    def setupGui(self):
        frame = QFrame(self)
        self.setCentralWidget(frame)
        self.grid_layout = QGridLayout(frame)

        self.map_canvas = QgsMapCanvas()
        self.grid_layout.addWidget(self.map_canvas)

    def add_ogr_layer(self, path):
        (name, ext) = os.path.basename(path).split('.')
        layer = QgsVectorLayer(path, name, 'ogr')
        self.project.addMapLayer(layer)
        self.map_canvas.setLayers([layer])
