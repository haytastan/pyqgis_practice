from PyQt5.QtWidgets import QApplication
from qgis.core import QgsApplication

from ourmainwindow_3 import OurMainWindow

app = QApplication([])
# set up QGIS
QgsApplication.setPrefixPath("/Users/gsherman/apps/QGIS.app/Contents/MacOS", True)
QgsApplication.initQgis()

# set the main window and show it
mw = OurMainWindow()
mw.show()

app.exec_()

# "delete" our main window
mw = None
# clean up QGIS
QgsApplication.exitQgis()
