from PyQt5 import QtWidgets

app = QtWidgets.QApplication([])
main_win = QtWidgets.QMainWindow()
frame = QtWidgets.QFrame(main_win)
main_win.setCentralWidget(frame)
grid_layout = QtWidgets.QGridLayout(frame)

text_editor = QtWidgets.QTextEdit()
text_editor.setText("This is a simple PyQt app that includes "
                    "a main window, a grid layout, and a text "
                    "editor widget.\n\n"
                    "It is constructed entirely from code.")
grid_layout.addWidget(text_editor)
main_win.show()
# Need the following statement if running as a script
app.exec_()
