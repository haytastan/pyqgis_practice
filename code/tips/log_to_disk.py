import os
import logging
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
  filename=os.path.join(os.environ['HOME'], 'my_qgis.log'),
  level=logging.DEBUG)

