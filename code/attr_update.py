layer = iface.activeLayer()
provider = layer.dataProvider()
features = layer.getFeatures(QgsFeatureRequest(1))
feature = features.next()
feature['name'] = 'My New Name'
feature['city'] = 'Seattle'
field_map = provider.fieldNameMap()
attrs = {field_map[key]: feature[key] for key in field_map}
layer.dataProvider().changeAttributeValues({feature.id(): attrs})

