# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Where Am i?
qgisMinimumVersion=3.0
description=Show location of a click on the map
version=0.1
author=GeoApt LLC
email=gsherman@geoapt.com

about=Show location of click on the map

tracker=http://bugs
repository=http://repo
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=python

homepage=http://homepage
category=Plugins
icon=whereami.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

