marker = QgsVertexMarker(iface.mapCanvas())
marker.setCenter(QgsPointXY(-151.0, 62.0))
marker.setIconSize(10)
marker.setIconType(QgsVertexMarker.ICON_BOX)
marker.setPenWidth(3)
marker.setColor(QColor(0, 255, 0))

markers = []

marker1 = QgsVertexMarker(iface.mapCanvas())
marker1.setCenter(QgsPointXY(-150.5, 62.0))
marker1.setIconType(QgsVertexMarker.ICON_CIRCLE)
markers.append(marker1)

marker2 = QgsVertexMarker(iface.mapCanvas())
marker2.setCenter(QgsPointXY(-150.5, 62.5))
marker2.setIconType(QgsVertexMarker.ICON_CIRCLE)
markers.append(marker2)

for marker in markers:
    iface.mapCanvas().scene().removeItem(marker)
