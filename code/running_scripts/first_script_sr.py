from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt
from qgis.core import QgsVectorLayer


class FirstScript:

    def __init__(self, iface):
        self.iface = iface

    def load_layer(self):
        wb = self.iface.addVectorLayer('/data/world_borders.shp', 'world_borders', 'ogr')

    def change_color(self):
        active_layer = self.iface.activeLayer()
        renderer = active_layer.renderer()
        symbol = renderer.symbol()
        symbol.setColor(QColor(Qt.red))
        active_layer.triggerRepaint()
        self.iface.layerTreeView().refreshLayerSymbology(active_layer.id())

    def open_attribute_table(self):
        self.iface.showAttributeTable(self.iface.activeLayer())


def run_script(iface):
    fs = FirstScript(iface)
    fs.load_layer()
    fs.change_color()
    fs.open_attribute_table()
