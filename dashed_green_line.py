sym = QgsLineSymbol.createSimple({
    'penstyle': 'dash',
    'color': 'green',
    'width': '4'
    })

layer = iface.activeLayer()
renderer = layer.renderer()
renderer.setSymbol(sym)
layer.triggerRepaint()
iface.layerTreeView().refreshLayerSymbology(layer.id())