layer = iface.activeLayer()
print(layer.name())
for f in layer.getFeatures():
    if f.isValid():
        geom = f.geometry()
        for p in geom.parts():
            print(p.asWkt())
    else:
        print('Feature is not valid.')