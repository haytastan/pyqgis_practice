from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt
from qgis.utils import iface

iface.addVectorLayer('C:/Users/Eron Lloyd/Projects/pyqgis_practice/pyqgis3_files/data/world_borders.shp',
    'world_border', 'ogr')

active_layer = iface.activeLayer()
renderer = active_layer.renderer()
symbol = renderer.symbol()
symbol.setColor(QColor(Qt.red))
active_layer.triggerRepaint()
iface.layerTreeView().refreshLayerSymbology(active_layer.id())

iface.showAttributeTable(iface.activeLayer())
